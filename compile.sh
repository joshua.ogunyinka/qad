BUILD_DIR="./builddir"
if [ -d "$BUILD_DIR" ]; then
    rm -rf $BUILD_DIR/*
else
    mkdir $BUILD_DIR
fi

meson setup $BUILD_DIR -Dbackend-ilm=true
meson compile -C $BUILD_DIR
cp $BUILD_DIR/qad qad
