# CI for this repo
We have a pursuit for `0` error, `0` warning, and `0` linting. Thus, this repo uses a strict CI: It treats all warnings as errors, even for format warnings.

To ensure any changes you've made meet the requirements of the CI, you can run these commands:
```shell
# To check and auto fix format issues.
clang-format -i src/*.c src/*/*/*.c src/*/*/*.h include/*.h

# To check lint warnings. Not all linting issues can be auto fixed so you have to solve them manually.
meson setup builddir && clang-tidy -p builddir -warnings-as-errors="*" src/*.c src/*/*/*.c src/*/*/*.h include/*.h
```

## If CI generates a false positives
If you are 100% confident `clang-format` and/or `clang-tidy` generates a false positive, then you can manually exclude them in the following way:

### For `clang-format` false positives
You can temporarily toggle off `clang-format` by writing comments in your source file:
```c
// clang-format off
    void unformatted_code;
// clang-format on
```
For further information, read documentation [here](https://clang.llvm.org/docs/ClangFormatStyleOptions.html#disabling-formatting-on-a-piece-of-code).

### For `clang-tidy` false positives
You can temporarily toggle off `clang-tidy` by writing comments in your source file:
```c
int unused_variable; // NOLINT
```
For further information, read documentation [here](https://clang.llvm.org/extra/clang-tidy/#suppressing-undesired-diagnostics).

## Tools version
Version for the tools CI uses is listed here, just in case they are useful.
```shell
clang-format --version
# Ubuntu clang-format version 14.0.0-1ubuntu1

clang-tidy --version
# Ubuntu LLVM version 14.0.0

meson --version
# 0.61.2

ninja --version
# 1.10.1
```
